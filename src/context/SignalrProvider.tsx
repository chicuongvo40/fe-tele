"use client";
import React, { createContext, useEffect, useState } from "react";
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";

export const SignalrContext = createContext<any>(undefined);

function SignalrProvider({ children }: { children: React.ReactNode }) {

  useEffect(() => {
    const initConnection = async () => {
      const signalrConnection = new HubConnectionBuilder()
        .withUrl("https://cuong.twentytwo.asia/chat")
        .withAutomaticReconnect()
        .configureLogging(LogLevel.Information)
        .build();
      await signalrConnection.start().then(() => {
        console.log("connected to signalr server");
      });
    };
    initConnection();
    // const initConnection = async () => {
    //   const socket = io("https://41pbhmgb-5000.asse.devtunnels.ms");
    //   setConnection(socket);
    // };
    // initConnection();
  }, []);
  return (
    <SignalrContext.Provider value={{ connection }}>
      {children}
    </SignalrContext.Provider>
  );
}

export default SignalrProvider;
