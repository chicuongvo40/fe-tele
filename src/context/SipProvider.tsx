
import React, { createContext, useEffect, useState } from "react";
import JsSIP from "jssip";


export const SipContext = createContext<SipContextValue>({
  userAgent: undefined,
});
function SipProvider({ children }: { children: React.ReactNode }) {

    

    const ua = new JsSIP.UA(config);

    ua.on("registrationFailed", async (ev) => {
      console.log(ev);
  
  }, []);
  return (
    <SipContext.Provider value={{ userAgent }}>{children}</SipContext.Provider>
  );
}

export default SipProvider;
