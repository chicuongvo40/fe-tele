"use client";
import { fetchApi } from "@/actions/fetch";

export default function Home() {
  const check = async () => {
    const data = await fetchApi("GET");

    console.log(data);
  };
  return (
    <main>{/* main content <button onClick={check}>check</button> */}</main>
  );
}
