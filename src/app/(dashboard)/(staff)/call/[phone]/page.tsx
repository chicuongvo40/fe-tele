"use client";
import Call from "@/components/Call";
import React from "react";

function page() {
  return (
    <div className="bg-white h-full grid grid-cols-2 grid-rows-2">
      <div className="border-2">hợp đồng</div>
      <Call />
      <div className="border-2"> lịch sử ticket</div>
      <div className="border-2">lịch sử các cuộc gọi trước của khách hàng</div>
    </div>
  );
}

export default page;
