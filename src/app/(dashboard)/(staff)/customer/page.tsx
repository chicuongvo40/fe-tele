import CustomerTable from "@/components/CustomerTable";
import DownloadBtn from "@/components/DownloadBtn";
import ImportExcel from "@/components/ImportExcel";
import Search from "@/components/Search";
import Link from "next/link";
import React from "react";

function Customer() {
  return (
    <div className="p-5 rounded-lg bg-white shadow-xl">
      <div className="flex items-center justify-between">
        <Search />
        <div className="gap-3 flex">
          <Link href="/">
            <button className="py-1 px-2 bg-[#5d57c9] text-white border-none rounded-md">
              Add New
            </button>
          </Link>
          <ImportExcel />
          <DownloadBtn />
        </div>
      </div>
      <CustomerTable />
    </div>
  );
}

export default Customer;
