import CustomSelect from "@/components/CustomSelect";
import TicketTable from "@/components/TicketTable";

function page() {
  return (
    <div className="p-5 rounded-lg bg-white shadow-xl">
      <CustomSelect />
      <TicketTable />
    </div>
  );
}

export default page;
