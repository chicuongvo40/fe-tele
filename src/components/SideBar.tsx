"use client";
import React from "react";
import { MdPerson, MdCall, MdConfirmationNumber } from "react-icons/md";
import MenuLink from "./MenuLink";
import Link from "next/link";
import { usePathname } from "next/navigation";

function SideBar() {
  const path = usePathname();

  const menuItems = [
    {
      title: "Customer",
      path: "/customer",
      icon: <MdPerson />,
    },
    {
      title: "Call history",
      path: "/callHistory",
      icon: <MdCall />,
    },
    {
      title: "Ticket",
      path: "/ticket",
      icon: <MdConfirmationNumber />,
    },
  ];
  return (
    !path.startsWith("/call/") && (
      <div className="flex h-screen ">
        <div className="bg-[#f4f5fa] text-black w-64 flex flex-col">
          <div className="p-4">
            <Link href="/" className="text-2xl font-bold">
              Sidebar
            </Link>
          </div>
          <div className="flex-1">
            {path.startsWith("/call/") ? (
              <ul className="px-4">
                {menuItems.map((item) => (
                  <li className="py-2" key={item.title}>
                    <span className="flex p-2 gap-2 rounded-xl items-center text-lg font-bold hover:bg-slate-200">
                      {item.icon}
                      {item.title}
                    </span>
                  </li>
                ))}
              </ul>
            ) : (
              <ul className="px-4">
                {menuItems.map((item) => (
                  <li className="py-2" key={item.title}>
                    <MenuLink item={item} />
                  </li>
                ))}
              </ul>
            )}
          </div>
        </div>
      </div>
    )
  );
}

export default SideBar;
