"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

type ItemPropType = {
  item: {
    title: string;
    path: string;
    icon: React.ReactElement;
  };
};
function MenuLink({ item }: ItemPropType) {
  const pathname = usePathname();

  return (
    <Link
      href={item.path}
      //   className={`${styles.container} ${
      //     pathname === item.path && styles.active
      //   }`}
      className={`flex p-2 gap-2 rounded-xl items-center text-lg font-bold hover:bg-slate-200 ${
        item.path === pathname && "bg-slate-200"
      }`}
      prefetch={false}
    >
      {item.icon}
      {item.title}
    </Link>
  );
}

export default MenuLink;
