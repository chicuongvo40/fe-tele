"use client";
import React from "react";
import { MdSearch } from "react-icons/md";

function Search() {
  const handleSearch = () => {};
  return (
    <div className="flex items-center gap-3 p-2 rounded-md w-max bg-[#f4f5fa]">
      <MdSearch />
      <input
        type="text"
        placeholder="Search for a user..."
        className="bg-transparent border-none text-black outline-none"
        onChange={handleSearch}
      />
    </div>
  );
}

export default Search;
