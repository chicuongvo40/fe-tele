"use client";
import React, { useContext } from "react";
import { Button } from "./ui/button";
import { SipContext } from "@/context/SipProvider";

function CallBtn({ phone }: { phone: string }) {
  const { userAgent } = useContext(SipContext);

  const handleCall = () => {
    // userAgent?.call(phone, {
    //   mediaConstraints: { audio: true, video: false },
    // });
    window.open(`/call/${phone}`, "_blank");
  };
  return (
    <Button
      size={"sm"}
      className="bg-teal-600 hover:bg-[#272e3f]"
      variant={"destructive"}
      onClick={handleCall}
    >
      Call
    </Button>
  );
}

export default CallBtn;
