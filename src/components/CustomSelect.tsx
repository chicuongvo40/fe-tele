"use client";
import React from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

function CustomSelect() {
  const handleChangeValue = (value: string) => {
    console.log(value);
  };

  return (
    <Select onValueChange={(value) => handleChangeValue(value)}>
      <SelectTrigger className="w-[180px] focus:ring-0 focus:ring-offset-0">
        <SelectValue placeholder="All" />
      </SelectTrigger>
      <SelectContent>
        <SelectItem value="all">All</SelectItem>
        <SelectItem value="open">Open</SelectItem>
        <SelectItem value="doing">Doing</SelectItem>
        <SelectItem value="close">Close</SelectItem>
      </SelectContent>
    </Select>
  );
}

export default CustomSelect;
