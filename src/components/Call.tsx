"use client";
import React from "react";
import { Button } from "./ui/button";
import { MdPhoneDisabled } from "react-icons/md";
import { MdArrowForward } from "react-icons/md";

function Call() {
  return (
    <div className="flex flex-col justify-between">
      <div className="mt-2 mx-2">
        <Button
          size={"sm"}
          variant={"destructive"}
          className="bg-teal-600 hover:bg-[#272e3f] float-right"
        >
          Create Ticket
        </Button>
        <div>
          <h3>Full Name:</h3>
          <h3>Phone:</h3>
          <h3>Adress:</h3>
        </div>
      </div>
      <div className="text-center">count</div>
      <div className="text-center mb-2">
        <Button
          size={"icon"}
          variant={"destructive"}
          className="rounded-[50%] mr-2"
        >
          <MdPhoneDisabled />
        </Button>
        <Button
          size={"icon"}
          variant={"destructive"}
          className="rounded-[50%] bg-teal-600 hover:bg-[#272e3f] absolute ml-2"
        >
          <MdArrowForward />
        </Button>
      </div>
    </div>
  );
}

export default Call;
