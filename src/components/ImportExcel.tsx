"use client";
import React from "react";

function ImportExcel() {
  const handleImport = () => {};
  return (
    <label className="py-1 px-2 bg-[#5d57c9] text-white border-none rounded-md cursor-pointer">
      Import from excel
      <input type="button" className="hidden" onChange={handleImport} />
    </label>
  );
}

export default ImportExcel;
