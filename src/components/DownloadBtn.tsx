"use client";
import React from "react";
import * as XLSX from "xlsx";

function DownloadBtn() {
  const handleDownloadFile = () => {
    const ws = XLSX.utils.aoa_to_sheet([
      ["First Name", "Last Name", "Phone", ["Address"]],
    ]);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    const excelBuffer = XLSX.write(wb, { type: "array" });
    const blobData = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    const url = window.URL.createObjectURL(blobData);

    const link = document.createElement("a");
    link.href = url;

    link.setAttribute("download", "template.xlsx");
    document.body.appendChild(link);

    link.click();

    link.remove();
    URL.revokeObjectURL(url);
  };
  return (
    <button
      className="py-1 px-2 bg-[#5d57c9] text-white border-none rounded-md"
      onClick={handleDownloadFile}
    >
      Download excel template
    </button>
  );
}

export default DownloadBtn;
