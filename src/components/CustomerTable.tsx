import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Button } from "@/components/ui/button";
import CallBtn from "./CallBtn";
function CustomerTable() {
  const data = [
    {
      id: 1,
      firsrName: "Bui Van",
      lastNane: "Thang",
      phone: "0378301007",
      address: "Thu Duc",
    },
  ];
  return (
    <Table className="mt-2">
      <TableHeader>
        <TableRow>
          <TableHead>No</TableHead>
          <TableHead>First Name</TableHead>
          <TableHead>Last Name</TableHead>
          <TableHead>Phone</TableHead>
          <TableHead>Address</TableHead>
          <TableHead></TableHead>
        </TableRow>
      </TableHeader>
      <TableBody>
        {data.map((user, index) => (
          <TableRow key={user.id}>
            <TableCell>{index + 1}</TableCell>
            <TableCell>{user.firsrName}</TableCell>
            <TableCell>{user.lastNane}</TableCell>
            <TableCell>{user.phone}</TableCell>
            <TableCell>{user.address}</TableCell>
            <TableCell className="flex gap-2">
              <Button
                size={"sm"}
                className="bg-teal-600 hover:bg-[#272e3f]"
                variant={"destructive"}
              >
                View
              </Button>
              <Button
                size={"sm"}
                className="bg-teal-600 hover:bg-[#272e3f]"
                variant={"destructive"}
              >
                Update
              </Button>
              <CallBtn phone={user.phone} />
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default CustomerTable;
