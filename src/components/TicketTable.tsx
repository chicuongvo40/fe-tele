import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
function TicketTable() {
  const data = [
    {
      id: 1,
      firsrName: "Bui Van",
      lastNane: "Thang",
      phone: "0378301007",
      address: "Thu Duc",
    },
  ];
  return (
    <Table className="mt-2">
      <TableHeader>
        <TableRow>
          <TableHead>No</TableHead>
          <TableHead>Customer Name</TableHead>
          <TableHead>Customer phone</TableHead>
          <TableHead>Staff name</TableHead>
          <TableHead>Type</TableHead>
          <TableHead>Survey rating</TableHead>
          <TableHead>Status</TableHead>
          <TableHead></TableHead>
        </TableRow>
      </TableHeader>
      <TableBody>
        {data.map((user, index) => (
          <TableRow key={user.id}>
            <TableCell>{index + 1}</TableCell>
            <TableCell>{user.firsrName}</TableCell>
            <TableCell>{user.lastNane}</TableCell>
            <TableCell>{user.phone}</TableCell>
            <TableCell>{user.address}</TableCell>
            <TableCell>4/5</TableCell>
            <TableCell>Doing</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default TicketTable;
