import React from "react";
import { Button } from "./ui/button";
import { MdLogout } from "react-icons/md";

function Header() {
  return (
    <div className="flex items-center justify-end gap-2">
      <h2>Hello,Bui Van Thang</h2>
      <Button size={"icon"} variant={"ghost"}>
        <MdLogout />
      </Button>
    </div>
  );
}

export default Header;
