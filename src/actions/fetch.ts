"use server";
type MethodType = "GET" | "PUT" | "POST" | "PATCH";
export async function fetchApi(
  method: MethodType,
  body?: string,
  token?: string
) {
  let data;
  if (token) {
    const res = await fetch(`${process.env.API_URL!}/products`, {
      method: method,
      body: body,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: token,
      },
    });
    data = await res.json();
  } else {
    const res = await fetch(`${process.env.API_URL!}/products`, {
      method: method,
      body: body,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    data = await res.json();
  }
  return data;
}
